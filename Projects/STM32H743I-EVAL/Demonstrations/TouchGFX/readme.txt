/**
  @page Demo   STM32H743I-EVAL ToucGFX Demonstration Firmware
 
  @verbatim
  ******************** (C) COPYRIGHT 2017 STMicroelectronics *******************
  * @file    Demonstration/readme.txt 
  * @author  MCD Application Team 
  * @brief   Description of STM32H743I-EVAL ToucGFX Demonstration
  ******************************************************************************
  @endverbatim

@par Demo Description

For more details please refer to the ToucGFX subsection in the full demonstration readme.txt file ../readme.txt

The binary and Media demonstration files are available from the STM32H743I-EVAL page in the Binary Delivery section.

Please refer to :

www.st.com/en/product/stm32h743i-eval.html


 * <h3><center>&copy; COPYRIGHT STMicroelectronics</center></h3>
 */
 
