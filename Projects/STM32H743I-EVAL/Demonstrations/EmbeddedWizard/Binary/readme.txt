/**
  @page Demo   STM32H743I-EVAL EmbeddedWizard Demonstration Firmware

@par Demo Description

The binary and Media demonstration files are available from the STM32H743I-EVAL page in the Binary Delivery section.

Please refer to :

www.st.com/en/product/stm32h743i-eval.html


 * <h3><center>&copy; COPYRIGHT STMicroelectronics</center></h3>
 */
 
